---
title: Nextcloud ShareMover App
summary: In this project, I developed and published a Nextcloud app in the Nextcloud app store. The app allows users to configure automatic moving of shares to a specific folder. It is developed with PHP in the backend and JavaScript / Vue.js in the frontend and I automated the whole building and publishing process with GitLab CI.
tags:
  - server
date: '2023-04-01T00:00:00Z'

# Optional external URL for project (replaces project detail page).
external_link: 'https://gitlab.com/frederikb96/nextcloud-mvsharedpics'

links:
  - icon: gitlab
    icon_pack: fab
    url: https://gitlab.com/frederikb96/nextcloud-mvsharedpics
    name: GitLab Repository
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
prio: 0

---