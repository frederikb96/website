---
title: Transformation - old city bike to randonneur
summary: How I transformed my old city bike into a randonneur.
tags:
  - bicycle
date: '2023-05-01T00:00:00Z'

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
prio: 10

---

{{< gallery album="gazelle" >}}