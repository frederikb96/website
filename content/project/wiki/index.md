---
title: Wiki [Archived]
summary: This is a knowledge base where I once wrote down stuff I learned about Linux servers, Nextcloud, and other self-hostable services.
tags:
  - server
date: '2022-06-01T00:00:00Z'

# Optional external URL for project (replaces project detail page).
external_link: 'https://wiki.bergrunde.net'

links:
  - name: Link to archived Wiki
    url: https://wiki.bergrunde.net
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
prio: 1

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
