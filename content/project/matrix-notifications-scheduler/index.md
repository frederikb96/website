---
title: Matrix Notification Scheduler
summary: This project is about a python script that can be used to automatically change for a Matrix account the notification profile of all rooms within a space
tags:
  - server
date: '2023-03-01T00:00:00Z'

# Optional external URL for project (replaces project detail page).
external_link: 'https://gitlab.com/frederikb96/matrix-notifications-spaces-scheduler'

links:
  - icon: gitlab
    icon_pack: fab
    url: https://gitlab.com/frederikb96/matrix-notifications-spaces-scheduler
    name: GitLab Repository
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
prio: 2

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---