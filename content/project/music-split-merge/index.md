---
title: Music Split and Merge
summary: This repository contains python scripts for manipulating old-school mp3 files.
tags:
  - server
date: '2021-12-01T00:00:00Z'

# Optional external URL for project (replaces project detail page).
external_link: 'https://gitlab.com/frederikb96/music-split-merge'

links:
  - icon: gitlab
    icon_pack: fab
    url: https://gitlab.com/frederikb96/music-split-merge
    name: GitLab Repository
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
prio: 4

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---