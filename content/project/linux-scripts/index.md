---
title: Linux Scripts
summary: A repository for many of my smaller projects, which don't have their own place.
tags:
  - server
date: '2020-04-01T00:00:00Z'

# Optional external URL for project (replaces project detail page).
external_link: 'https://gitlab.com/frederikb96/Linux-Scripts'

links:
  - icon: gitlab
    icon_pack: fab
    url: https://gitlab.com/frederikb96/Linux-Scripts
    name: GitLab Repository
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
prio: 3

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---