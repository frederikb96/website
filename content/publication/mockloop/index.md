---
title: 'A fully automated normothermic machine perfusion system for kidney grafts supporting physiological motivated flow profiles'

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here
# and it will be replaced with their full name and linked to their profile.
authors:
  - Marc Wiartalla
  - admin
  - Jahnn Kühn
  - Mateusz Buglowski
  - Stefan Kowalewski
  - André Stollenwerk
  - Christian Bleilevens

date: '2023-09-01T00:00:00Z'
doi: 'https://doi.org/10.1515/cdbme-2023-1081'


# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ['1']

# Publication name and optional abbreviated publication name.
publication: Current Directions in Biomedical Engineering
publication_short: BMT

abstract: Research showed that the normothermic machine perfusion of kidneys can enable prolonged storage and improve conditions compared to traditional cold storage. For research in this area, there is a demand for a long-term in vitro perfusion setup. In this work, we present a fully automated normothermic machine perfusion (NMP) system as an experimental research platform. The perfusion system is intended as a tool for researching the effects of different perfusion strategies on the kidney. To enable the automation, the NMP system consists of a blood pressure control, a circulation volume level control and a pH-regulation component. The setup is realized as a medical cyber-physical system consisting of networked embedded microcontroller nodes.

tags: []
prio: 2

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: 'https://www.degruyter.com/document/doi/10.1515/cdbme-2023-1081/pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Normothermic machine perfusion system'
  focal_point: ''
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
#projects:
#  - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
#---

#{{% callout note %}}
#Click the _Cite_ button above to demo the feature to enable visitors to import publication metadata into their reference management software.
#{{% /callout %}}

#{{% callout note %}}
#Create your slides in Markdown - click the _Slides_ button to check out the example.
#{{% /callout %}}

#Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/).
---
