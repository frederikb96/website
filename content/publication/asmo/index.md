---
title: 'ASMO: a decentralized and verifiable interoperability platform in intensive care'

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here
# and it will be replaced with their full name and linked to their profile.
authors:
  - admin
  - Marc Wiartalla
  - Moritz Hüllnann
  - Andreas Derks
  - Stefan Kowalewski
  - André Stollenwerk

date: '2023-03-30T00:00:00Z'
doi: 'https://doi.org/10.18416/AUTOMED.2023'

# Schedule page publish date (NOT publication's date).

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ['1']

# Publication name and optional abbreviated publication name.
publication: Proceedings on automation in medical engineering
publication_short: AUTOMED

abstract: Interconnected medical devices enable new therapies and automate existing ones. Due to various manufacturers and interfaces, interoperability needs to be enabled with the help of auxiliary hardware. Since functional safety is indisputably critical, verifiability is essential, which is often neglected by state-of-the-art medical hardware platforms. We propose the ASMO hardware platform, which provides various interfaces to enable interoperability and where the workload is distributed such that the complexity of each unit can be reduced, while still providing enough capabilities for embedded machine learning. By using microcontrollers running an embedded real-time operating system, the verifiability can be further increased. The intrinsically created distributed architecture additionally allows for flexible rearrangement and efficient extension if needed.

tags: []
prio: 0

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: 'https://www.journals.infinite-science.de/index.php/automed/article/view/724/401'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'ASMO hardware platform'
  focal_point: ''
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
#projects:
#  - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
#---

#{{% callout note %}}
#Click the _Cite_ button above to demo the feature to enable visitors to import publication metadata into their reference management software.
#{{% /callout %}}

#{{% callout note %}}
#Create your slides in Markdown - click the _Slides_ button to check out the example.
#{{% /callout %}}

#Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/).
---
