---
title: EA Germany Retreat 2023

event: EA Germany Retreat

summary: In August, I attended the EA Germany retreat 2023 and made some new connections and was looking for new job opportunities.

date: '2023-08-04T00:00:00Z'
date_end: '2023-08-06T00:00:00Z'
all_day: true

tags: [EA]

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
---