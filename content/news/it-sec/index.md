---
title: Cybersecurity - Certificates List

event: Cybersecurity

summary: Over the last few months, I have been focusing on cybersecurity and information security. I attended a lecture on network security and cryptography at the RWTH Aachen University. Furthermore, I acquired further practical expertise by completing the TryHackMe learning paths to attain certifications and taking part in challenges. Moreover, I completed the Google Cybersecurity Certificate and aquired the CompTIA Security+ certificate. See more...

date: '2024-03-27T00:00:00Z'
date_end: '2024-03-27T00:00:00Z'
all_day: true

tags: [Security, EA]

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
---

Over the last few months, I have been focusing on cybersecurity and information security. I attended a lecture on [network security and cryptography](https://online.rwth-aachen.de/RWTHonline/ee/ui/ca2/app/desktop/#/slc.tm.cp/student/courses/483913?$ctx=design=ca;lang=en&$scrollTo=toc_overview) at the RWTH Aachen University. Furthermore, I acquired further practical expertise by completing the TryHackMe learning paths to attain certifications and taking part in challenges. Moreover, I completed the Google Cybersecurity Certificate and aquired the CompTIA Security+ certificate.

# Certificates
- [CompTIA Security+](/uploads/security+.pdf)
- [Google Cybersecurity Certificate](/uploads/cyber-cert.pdf)
- [TryHackMe - Introduction to Cybersecurity](/uploads/THM-B8W6MHUBRP-intro.pdf)
- [TryHackMe - Pre Security](/uploads/THM-PXQRXJKQNF-pre-security.pdf)