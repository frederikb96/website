---
title: Maschine Learning for Good - AI Alignment Camp

event: ML4Good
event_url: https://germany.ml4good.org/

summary: In August, I attended the ML4Good camp and succesfully completed it, see [accomplishments](#accomplishments) and the [certificate](uploads/ML4Good_Certificate_Frederik_Berg.pdf)

date: '2023-08-23T00:00:00Z'
date_end: '2023-09-03T00:00:00Z'
all_day: true

tags: [EA]

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
---