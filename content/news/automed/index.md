---
title: Talk at AUTOMED Conference 2023

event: AUTOMED Conference
event_url: https://www.thm.de/lse/en/forschung/ibmt/ag-biomedizinische-signale-und-systeme/automed-2023.html

summary: In March, I gave a talk on my [publication about the ASMO hardware platform](./publication/asmo) at the AUTOMED Conference 2023.

date: '2023-03-30T00:00:00Z'
#date_end: '2023-03-30T00:00:00Z'
all_day: true

tags: [talk]

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''


---