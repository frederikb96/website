---
title: Nextcloud Conference and Contributor Week 2023

event: Nextcloud Conference
event_url: https://nextcloud.com/conference-2023/

summary: In September, I attended the Nextcloud Conference and the Contributor Week and continued to work on my [Nextcloud Share Config App](./project/nc-mvsharedpics).

date: '2023-09-16T00:00:00Z'
date_end: '2023-09-22T00:00:00Z'
all_day: true

tags: [server]

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''

projects:
  - nc-mvsharedpics
---