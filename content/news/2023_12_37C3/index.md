---
title: Chaos Communciation Congress 2023

event: Chaos Communication Congress
event_url: https://events.ccc.de/congress/2023/infos/startpage.html

summary: In December, I attended the Chaos Communication Congress 2023. It was my first time at a CCC and I am amazed by the community and the talks. I also got new insights into interesting job opportunities and attended workshops on security, privacy and penetration testing.

date: '2023-12-27T00:00:00Z'
date_end: '2023-12-30T00:00:00Z'
all_day: true

tags: [Security]

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
---