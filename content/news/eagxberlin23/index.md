---
title: EAGx Berlin Conference 2023

event: EAGx Berlin Conference
event_url: https://www.effectivealtruism.org/ea-global/events/eagxberlin-2023

summary: In September, I attended the EAGx Berlin Conference 2023 and further investigated how to have a positive impact with a job in the field of Information Security.

date: '2023-09-09T00:00:00Z'
date_end: '2023-09-10T00:00:00Z'
all_day: true

tags: [EA]

links:
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
---