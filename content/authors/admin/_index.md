---
# Display name
title: Frederik Berg

# Full name (for SEO)
first_name: Frederik
last_name: Berg

# Status emoji
status:
  icon: ☕️

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Security Analytics Consultant

# Organizations/Affiliations to show in About widget
organizations:
  - name: SVA System Vertrieb Alexander GmbH
    url: https://www.sva.de/

# Short bio (displayed in user profile at end of posts)
bio: Background in Cybersecurity and conducted research on safety of embedded medical devices.

# Interests to show in About widget
interests:
  - Cybersecurity
  - Embedded Systems
  - AI Safety
  - Linux Server

# Education to show in About widget
education:
  courses:
    - course: Computer Science Continuing Education
      institution: RWTH Aachen University
      year: 2022 - 2024
    - course: M.Sc. in Automation Engineering
      institution: RWTH Aachen University
      year: 2019 - 2022
    - course: B.Sc. in Mechanical Engineering
      institution: Leibniz University Hannover
      year: 2014 - 2019

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: '/#contact'
    display:
      header: true
  - icon: graduation-cap # Alternatively, use `google-scholar` icon from `ai` icon pack
    icon_pack: fas
    link: https://scholar.google.co.uk/citations?user=cmAmnmcAAAAJ
  - icon: gitlab
    icon_pack: fab
    link: https://gitlab.com/frederikb96
  - icon: github
    icon_pack: fab
    link: https://github.com/frederikb96
  # Link to a PDF of your resume/CV.
  # To use: copy your cv to `static/uploads/cv.pdf`, enable `ai` icons in `params.yaml`,
  # and uncomment the lines below.
  - icon: cv
    icon_pack: ai
    link: uploads/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: true
---

I am working as a **Security Analytics Consultant** at [SVA System Vertrieb Alexander GmbH](https://www.sva.de/). Previously, I was a **Research Assistant** at the Chair for **Embedded Software** at RWTH Aachen University. My research was about the **safety** of embedded medical devices. Over the last year, my interest has increasingly shifted towards **cybersecurity**, a field of high-impact and crucial importance for both the present and the future. Moreover, I am also engaged in some personal security and server projects. I am happy to work in this dynamic and changing area. My strengths include a **strong motivation to learn** and develop, the ability to solve **complex problems**, exceptional **organizational skills**, and an emphasis on well-documented work.
{style="text-align: justify;"}

For a few years, I have been working on projects like **Linux servers** including Nextcloud, many more self-hostable services and network/backup solutions. Moreover, I am interested in **knowledge management**, as well as organizing and structuring data. On the sport side, I like bouldering and being in nature, bicycling and hiking. Furthermore, I enjoy working on the refurbishment of old bicycles.
{style="text-align: justify;"}